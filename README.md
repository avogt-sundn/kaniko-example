

# kubernetes-quickstart project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `kubernetes-quickstart-1.0.0-SNAPSHOT-runner.jar` file in the `/target` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/lib` directory.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application is now runnable using `java -jar target/kubernetes-quickstart-1.0.0-SNAPSHOT-runner.jar`.

## Creating an image

We will package the Quarkus app into a jar file and embed
that into a pre-existing base image. 
* That can be run on any Docker or Kubernetes environment.
* Quarkus runs in jvm mode, the OpenJDK is included in the base image

On Windows/Mac:
* you run the native executable build in a container using:
   ```shell script
   ./mvnw package \
                  -Dquarkus.kubernetes.deploy=true \
                  -Dquarkus.kubernetes.namespace=default \
                  -DskipTests \
                  -Dquarkus.kubernetes.image-pull-policy=IfNotPresent

   ```

  > On Apple Silicon an amd64 image will be used. But you can force usage of an arm64 jvm base image by adding the platform argument    `-Dquarkus.jib.platforms=linux/arm64/v8` .


On Linux:
* you run the native executable build natively:
   ```shell script
   ./mvnw package -Pnative \
                  -Dquarkus.kubernetes.deploy=true \
                  -DskipTests
   ```

## Creating an image with Quarkus native executable

Again, we build the image to be deployed to Kubernetes. But this time the Quarkus app will be compiled into a native binary:
* Linux binary, runs on linux servers, Kubernetes clusters and in the Docker environment
* the binary will have the same architecture as the processor it was created on.

On Windows/WSL2/Mac:
* you run the native executable build in a container using: 
   ```shell script
   ./mvnw package -Pnative \
                  -Dquarkus.kubernetes.deploy=true \
                  -Dquarkus.native.container-build=true \
                  -DskipTests \
                  -Dquarkus.kubernetes.image-pull-policy=IfNotPresent
   ```
- You cannot run the linux binary on Windows/Mac, but you can inspect your native executable with the file command:
  ```shell script
  file ./target/kubernetes-quickstart-1.0.0-SNAPSHOT-runner`
  # target/quarkus-kubernetes-quickstart-1.0.0-SNAPSHOT-runner: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=f74f37d2b61650899404c8c5e23172de34795bb4, not stripped
  ```

On WSL2/Linux:
- You can create a native executable using:
   ```shell script
   ./mvnw package -Pnative
   ```
- You can immediately execute it
  ```shell script
  ./target/kubernetes-quickstart-1.0.0-SNAPSHOT-runner`
  #__  ____  __  _____   ___  __ ____  ______
  #--/ __ \/ / / / _ | / _ \/ //_/ / / / __/
  #-/ /_/ / /_/ / __ |/ , _/ ,< / /_/ /\ \
  #--\___\_\____/_/ |_/_/|_/_/|_|\____/___/
  #2022-01-24 19:44:23,447 INFO  [io.quarkus] (main) quarkus-kubernetes-quickstart 1.0.0-SNAPSHOT native (powered by Quarkus 2.7.1.Final) started in 0.030s. Listening on: http://0.0.0.0:8080
  #2022-01-24 19:44:23,447 INFO  [io.quarkus] (main) Profile prod activated.
  #2022-01-24 19:44:23,447 INFO  [io.quarkus] (main) Installed features: [cdi, kubernetes, resteasy, resteasy-jackson, smallrye-context-propagation, smallrye-openapi, vertx]
  ```
  
If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.html.

## Verifying the deployment

> The following commands declare no namespace parameter thus accessing the default 
> namespace where the former quarkus deploy was targeted at.
 


1. Is the pod running?
   * ```bash
     kubectl get pod
     #NAME                                             READY   STATUS    RESTARTS   AGE
     #quarkus-kubernetes-quickstart-78cbc9bc9f-fn8lw   1/1     Running   0          45m
     ```

2. Is the deployment running?
   * ```bash
     kubectl get deployment
     #NAME                            READY   UP-TO-DATE   AVAILABLE   AGE
     #quarkus-kubernetes-quickstart   1/1     1            1           50m```
     ```

3. Which image does the pod/deployment use?
   * ```bash
     kubectl get pod/quarkus-kubernetes-quickstart-78cbc9bc9f-fn8lw -o json |grep image
     # "image": "avogt/quarkus-kubernetes-quickstart:1.0.0-SNAPSHOT",
     # "imagePullPolicy": "IfNotPresent",
     # "image": "avogt/quarkus-kubernetes-quickstart:1.0.0-SNAPSHOT",
     # "imageID": "docker://sha256:0b842134f2971d4986a33e2a2975bd743689cdc272210436865dadc819c8cc27",
     ```
4. When was that image created? 
   * ```bash
     docker inspect avogt/quarkus-kubernetes-quickstart:1.0.0-SNAPSHOT|grep  Created
     # "Created": "2022-01-24T18:25:49.853522Z",
     ```
5. What is it's unique id?
    * compare that with the results from inspecting the pod:
    * ```bash
      kubectl get pod/quarkus-kubernetes-quickstart-78cbc9bc9f-fn8lw -o json |grep imageID
      # "imageID": "docker://sha256:0b842134f2971d4986a33e2a2975bd743689cdc272210436865dadc819c8cc27",
      
      docker inspect avogt/quarkus-kubernetes-quickstart:1.0.0-SNAPSHOT|grep Id
      # "Id": "sha256:0b842134f2971d4986a33e2a2975bd743689cdc272210436865dadc819c8cc27",
      ```
      

# RESTEasy JAX-RS

<p>A Hello World RESTEasy resource</p>

Guide: https://quarkus.io/guides/rest-json

# Using gitlab pipelines to automate building

```bash
kubectl delete ns gitlab-space; kubectl create ns gitlab-space ; TOKEN=_XJqDK5n297vKqyjyxiz ; \
  helm install --namespace gitlab-space the-runner \
    -f gitlab-runner-helm-values.yaml \
    --set gitlabUrl=https://gitlab.com,runnerRegistrationToken=$TOKEN \
    gitlab/gitlab-runner
```
## Create a minio cache

First, we need an ip that is accessable from any container running on your machine. MinIO container will then be assigned to that address.

Either,
1. Find your computer's public ip
2. assign it a name

    ```Bash
    PUBLIC_IP=$(/sbin/ifconfig eth0 | grep 'inet ' | awk '{ print $2 }') ; echo 'my ip is: '$PUBLIC_IP
    # 10.10.2.113
    sudo sh -c "echo $PUBLIC_IP minio |tee -a /etc/hosts"
    #
    #10.10.2.113 minio
    ```
   
**or** use the docker default hostname `host.docker.internal` 
* that requires you to change the address** in the [gitlab-runner-helm-values.yaml](gitlab-runner-helm-values.yaml)

1. check that it resolves on your computer:
    ```Bash
    ping host.docker.internal
    #
    #PING host.docker.internal (10.10.2.113) 56(84) bytes of data.
    #64 bytes from host.docker.internal (10.10.2.113): icmp_seq=1 ttl=64 time=0.033 ms
    ```
    
2. check that it resolves within a container:

   ```bash
   docker run --rm alpine ping host.docker.internal
   #
   #PING host.docker.internal (192.168.65.2): 56 data bytes
   #64 bytes from 192.168.65.2: seq=0 ttl=37 time=9.683 ms
    ```
We must use the chosen ip or hostname in the gitlab-runner configuration, 
the gitlab-runner   

## Create 
Create a container running minio on port 9000 of your computer
```bash
mkdir -p $HOME/.minio/export
docker run -d --restart always -p 9000:9000 \
    -v $HOME/.minio:/root/.minio -v $HOME/.minio/export:/export \
    -e "MINIO_ROOT_USER=admin" \
    -e "MINIO_ROOT_PASSWORD=minio123" \
    --name minio \
  minio/minio:latest server /export```

sudo echo "10.11.12.24   minio" >> /etc/hosts

